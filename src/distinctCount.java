import java.io.IOException;
import java.util.*;
        
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

/** 學號 5106056035 張朝任
 * 這邊分兩個JOB
 * 1. 先將資料用數字做KEY
 * 2. 將資料統計數目  */
public class distinctCount {

    //第一工作:讀取資料用KEY分類
    public static class firstMap extends Mapper<LongWritable, Text, Text, IntWritable> {
        //private final static IntWritable one = new IntWritable(1);
        //private Text word = new Text();

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                
                //word.set(tokenizer.nextToken());
                //context.write(word, one);
                context.write(new Text(tokenizer.nextToken()), new IntWritable(1));
            }
        }
    } 

    public static class firstReduce extends Reducer<Text, IntWritable, Text, IntWritable> {

        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            /*
            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }
            */
            //context.write(key, new IntWritable(sum));
            context.write(key, new IntWritable(1));
        }
    }

    //第二工作:讀取分類資料計算數量
    public static class secondMap extends Mapper<LongWritable, Text, Text, IntWritable> {

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);

            //讀取第一個值 是分類後的數值
            //String diffNumber = tokenizer.nextToken();
            //String token2 = tokenizer.nextToken();

            context.write(new Text("distinct Count is:"),new IntWritable(1));
        }
    }

    public static class secondReduce extends Reducer<Text, IntWritable, Text, IntWritable> {

        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {

            int sum = 0;
            for (IntWritable val : values) {
                //sum += val.get();
                sum += 1;
            }
            //寫入總數
            context.write(new Text(key), new IntWritable(sum));
        }
    }

    public static void main(String[] args) throws Exception {

        //第一個工作設定
        Configuration conf = new Configuration();
        Job job = new Job(conf, "distinctCount first");   
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.setMapperClass(firstMap.class);
        job.setReducerClass(firstReduce.class);
        job.setJarByClass(distinctCount.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        job.waitForCompletion(true);

        //第二個工作設定
        Configuration conf2 = new Configuration();
        Job job2 = new Job(conf, "distinctCount second");
        job2.setOutputKeyClass(Text.class);
        job2.setOutputValueClass(IntWritable.class);
        job2.setMapperClass(secondMap.class);
        job2.setReducerClass(secondReduce.class);
        job2.setJarByClass(distinctCount.class);
        job2.setInputFormatClass(TextInputFormat.class);
        job2.setOutputFormatClass(TextOutputFormat.class);
        FileInputFormat.addInputPath(job2, new Path(args[1]));
        FileOutputFormat.setOutputPath(job2, new Path(args[2]));
        job2.waitForCompletion(true);
    }
}
