import java.io.IOException;
import java.util.*;
import java.text.DecimalFormat; 
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

/** 學號 5106056035 張朝任
 * 作業2 資料清洗
 * 1. 切開逗點資料
 * 2. 取代錯誤資料
 * 3. 重新寫入  */
public class work2DataClean {

    //切開逗點資料+取代錯誤資料
    public static class firstMap extends Mapper<LongWritable, Text, Text, Text> {
        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line     = value.toString();
            String[] tokens = line.split(",");
            String lineKey  = tokens[0]+tokens[1]+tokens[2];
            double lineT    = 0;//用平均補上數值
            double lineC    = 0;//計算次數
            for (int i = 0; i < tokens.length; i++)
            {
                //前面三個不處理
                if (i <= 2)continue;
                //NR為0
                if (tokens[i].length() >= 2 && tokens[i].substring(0,2) == "NR")
                {
                    tokens[i] = "0";
                }
                //-開頭變成0
                if (tokens[i].length() >= 1 && tokens[i].substring(0,1) == "-")
                {
                    tokens[i] = "0";
                }
                //取代不是數字或是.
                tokens[i] = tokens[i].replaceAll("([^0-9.])","");
                if (!tokens[i].isEmpty() && Double.parseDouble(tokens[i]) > 0 )
                {
                    lineT += Double.parseDouble(tokens[i]);
                    lineC += 1;
                }
            }
            String addStr = "";
            //如果有平均數 則取代
            if (lineT > 0 && lineC > 0 )
            {
                lineT = lineT/lineC;
                DecimalFormat df = new DecimalFormat("0.00");
                addStr    = df.format(lineT);
            }
            //沒有都用0
            else
            {
                addStr    = "0";
            }
            for (int i = 0; i < tokens.length; i++)
            {
                if (tokens[i].isEmpty())
                {
                    tokens[i] = addStr;
                }
            }

            //產生新字串
            String resultStr = String.join(",",tokens);

            //寫入資料
            context.write(new Text(resultStr),new Text(""));
        }
    } 

    //重新寫入
    public static class firstReduce extends Reducer<Text, Text, Text, Text> {

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            //直接寫入
            for (Text val : values) {
                context.write(key,val);
            }
        }
    }

    public static void main(String[] args) throws Exception {

        //工作設定
        Configuration conf = new Configuration();
        Job job = new Job(conf, "work2DataClean");   
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setMapperClass(firstMap.class);
        job.setReducerClass(firstReduce.class);
        job.setJarByClass(work2DataClean.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        job.waitForCompletion(true);
    }
}
